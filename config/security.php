<?php

return [
    'content' => [
        'default' => 'global',
        'profiles' => [
            'global' => [
                'base-uri' => "'self'",
                'default-src' => "'self'",
                'font-src' => [
                    "'self'",
                    'fonts.gstatic.com'
                ],
                'img-src' => [
                    "'self'",
                    //env('APP_URL'),
                ],
                'script-src' => [
                    "'self'",
                    "'unsafe-inline'",
                    'https://www.google-analytics.com',
                ],
                'style-src' => [
                    "'self'",
                    "'unsafe-inline'",
                    'https://fonts.googleapis.com'
                ],
            ],
            'vimeo_player' => [
                'media-src' => [
                    "'self'",
                    'https://player.vimeo.com'
                ],
                'child-src' => [
                    "'self'",
                    'https://player.vimeo.com'
                ]
            ]
        ],
    ],
];
