<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmProjectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_projections', function (Blueprint $table) {
            $table->increments('id');

            $table->date('starts_at');
            $table->date('ends_at')->nullable();
            $table->string('ticket_purchase_url')->nullable();
            $table->boolean('published');

            $table->unsignedInteger('film_id');
            $table->unsignedInteger('cinema_id');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');

            $table->foreign('cinema_id')
                ->references('id')
                ->on('cinemas');

            $table->timestamps();
        });

        Schema::create('film_projection_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_projection_id');
            $table->string('locale')->index();

            $table->string('info')->nullable();

            $table->unique(['film_projection_id', 'locale']);
            $table->foreign('film_projection_id')
                ->references('id')
                ->on('film_projections')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_projection_translations');
        Schema::dropIfExists('film_projections');
    }
}
