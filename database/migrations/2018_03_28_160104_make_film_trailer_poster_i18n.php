<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeFilmTrailerPosterI18n extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('film_translations', function (Blueprint $table) {
            $table->text('trailer')->nullable();
            $table->string('poster_file_path')->nullable();
        });

        DB::statement(
            "UPDATE film_translations ft " .
            "LEFT JOIN films f ON ft.film_id=f.id " .
            "SET ft.trailer = f.trailer, ft.poster_file_path = f.poster_file_path"
        );

        Schema::table('films', function (Blueprint $table) {
            $table->dropColumn('trailer');
            $table->dropColumn('poster_file_path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->text('trailer')->nullable();
            $table->string('poster_file_path')->nullable();
        });

        DB::statement(
            "UPDATE films AS f, film_translations AS ft " .
            "SET f.trailer = ft.trailer, f.poster_file_path = ft.poster_file_path " .
            "WHERE f.id = ft.film_id AND ft.locale = 'es'"
        );

        Schema::table('film_translations', function (Blueprint $table) {
            $table->dropColumn('trailer');
            $table->dropColumn('poster_file_path');
        });


    }
}
