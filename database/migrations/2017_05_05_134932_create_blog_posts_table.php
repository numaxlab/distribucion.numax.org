<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('image_file_path');
            $table->dateTime('published_at');

            $table->timestamps();
        });

        Schema::create('blog_post_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('blog_post_id');
            $table->string('locale')->index();

            $table->string('title');
            $table->string('slug');
            $table->text('intro');
            $table->text('body');

            $table->unique(['blog_post_id', 'locale']);
            $table->foreign('blog_post_id')
                ->references('id')
                ->on('blog_posts')
                ->onDelete('cascade');
        });

        Schema::create('blog_post_film', function (Blueprint $table) {
            $table->unsignedInteger('blog_post_id');
            $table->unsignedInteger('film_id');

            $table->foreign('blog_post_id')
                ->references('id')
                ->on('blog_posts')
                ->onDelete('cascade');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post_film');
        Schema::dropIfExists('blog_post_translations');
        Schema::dropIfExists('blog_posts');
    }
}
