<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFilmsFilmMakersRelationshipType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->dropForeign('films_film_maker_id_foreign');
            $table->dropColumn('film_maker_id');
        });

        Schema::create('film_film_maker', function (Blueprint $table) {
            $table->unsignedInteger('film_id');
            $table->unsignedInteger('film_maker_id');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');

            $table->foreign('film_maker_id')
                ->references('id')
                ->on('film_makers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_film_maker');

        Schema::table('films', function (Blueprint $table) {
            $table->unsignedInteger('film_maker_id');

            $table->foreign('film_maker_id')
                ->references('id')
                ->on('film_makers');
        });
    }
}
