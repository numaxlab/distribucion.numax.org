<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_resources', function (Blueprint $table) {
            $table->increments('id');

            $table->string('file_path')->nullable();
            $table->string('url')->nullable();

            $table->unsignedInteger('film_id');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('film_resource_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_resource_id');
            $table->string('locale')->index();

            $table->string('name');

            $table->unique(['film_resource_id', 'locale']);
            $table->foreign('film_resource_id')
                ->references('id')
                ->on('film_resources')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_resource_translations');
        Schema::dropIfExists('film_resources');
    }
}
