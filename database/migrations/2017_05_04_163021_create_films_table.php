<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('year');
            $table->integer('minutes');
            $table->string('classification');
            $table->text('exhibition_formats');
            $table->string('production')->nullable();
            $table->text('cast')->nullable();
            $table->text('trailer')->nullable();
            $table->string('header_image_file_path')->nullable();
            $table->string('poster_file_path')->nullable();
            $table->date('release_date')->nullable();
            $table->boolean('published');

            $table->timestamps();
        });

        Schema::create('film_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_id');
            $table->string('locale')->index();

            $table->string('title');
            $table->string('slug');
            $table->string('original_language');
            $table->string('subtitles')->nullable();
            $table->string('origin_countries');
            $table->text('synopsis')->nullable();

            $table->unique(['film_id', 'locale']);
            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_translations');
        Schema::dropIfExists('films');
    }
}
