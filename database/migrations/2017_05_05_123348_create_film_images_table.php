<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_images', function (Blueprint $table) {
            $table->increments('id');

            $table->string('file_path');

            $table->unsignedInteger('film_id');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('film_image_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_image_id');
            $table->string('locale')->index();

            $table->string('name')->nullable();

            $table->unique(['film_image_id', 'locale']);
            $table->foreign('film_image_id')
                ->references('id')
                ->on('film_images')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_image_translations');
        Schema::dropIfExists('film_images');
    }
}
