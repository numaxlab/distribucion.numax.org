<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmStreamingMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_streaming_media', function (Blueprint $table) {
            $table->increments('id');

            $table->string('url')->nullable();
            $table->text('embed')->nullable();
            $table->boolean('starred');

            $table->unsignedInteger('film_id');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('film_streaming_media_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_streaming_media_id');
            $table->string('locale')->index();

            $table->string('name');

            $table->unique(['film_streaming_media_id', 'locale'], 'fsm_translations_fsm_id_locale_unique');
            $table->foreign('film_streaming_media_id', 'fsm_translations_fsm_id_foreign')
                ->references('id')
                ->on('film_streaming_media')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_streaming_media_translations');
        Schema::dropIfExists('film_streaming_media');
    }
}
