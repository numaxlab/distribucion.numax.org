<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Packer;
use Route;

class ViewHelper
{
    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $attribute
     * @param string $transform
     * @return string
     */
    public function imgSrc(Model $model, $attribute, $transform = null)
    {
        $image = $model->getAttribute($attribute);

        if (strpos($image, '/') !== 0) {
            $image = 'storage/'.$image;
        }

        if (empty($transform)) {
            return asset($image);
        }

        return Packer::img($image, $transform);
    }

    /**
     * @param $file
     * @return string
     */
    public function fileHref($file)
    {
        return asset('storage/'.$file);
    }

    /**
     * @param $locale
     * @param null $routeName
     * @param array $parameters
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getLocalizedUrl($locale, $routeName = null, array $parameters = [])
    {
        if (empty($routeName)) {
            $routeName = Route::getCurrentRoute()->getName();
        }
        $route = __('routes.'.$routeName, [], $locale);

        if (empty($parameters)) {
            $parameters = Route::getCurrentRoute()->parameters();
        }

        if (! empty($parameters)) {
            foreach ($parameters as $name => $value) {
                $route = str_replace('{'.$name.'}', $value, $route);
            }
        }

        return url($route);
    }

    /**
     * @param $videoData
     * @param $width
     * @param $height
     * @return string
     */
    public function videoEmbed($videoData, $width, $height)
    {
        if ($videoData['provider'] == 'vimeo') {
            return $this->vimeoEmbed($videoData['url'], $width, $height);
        } else if ($videoData['provider'] == 'youtube') {
            return $this->youtubeEmbed($videoData['url'], $width, $height);
        }

        return '';
    }

    private function vimeoEmbed($url, $width, $height)
    {
        $endpoint = 'https://vimeo.com/api/oembed.json?url=%s&width=%s&height=%s';

        $requestUrl = sprintf($endpoint, $url, $width, $height);

        $oembed = $this->curlGet($requestUrl);
        $oembed = json_decode($oembed, true);

        if (! empty($oembed) && is_array($oembed) && array_key_exists('html', $oembed)) {
            return $oembed['html'];
        }

        return '';
    }

    private function youtubeEmbed($url, $width, $height)
    {
        $endpoint = 'https://www.youtube.com/oembed?url=%s&maxwidth=%s&maxheight=%s&format=json';

        $requestUrl = sprintf($endpoint, $url, $width, $height);

        $oembed = $this->curlGet($requestUrl);
        $oembed = json_decode($oembed, true);

        if (! empty($oembed) && is_array($oembed) && array_key_exists('html', $oembed)) {
            return $oembed['html'];
        }

        return '';
    }

    private function curlGet($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return $return;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $filmMakers
     * @param bool $withLinks
     * @return string
     */
    public function showFilmFilmMakers(Collection $filmMakers, $withLinks = false)
    {
        $markup = '';

        foreach ($filmMakers as $maker) {
            if ($maker == $filmMakers->last() && $maker != $filmMakers->first()) {
                $markup .= ' ' . __('messages.y') . ' ';
            } elseif ($maker != $filmMakers->first()) {
                $markup .= ', ';
            }

            if ($withLinks) {
                $markup .= sprintf('<a href="%s" itemprop="url">%s</a>', route('film-maker.show', $maker->slug), $maker->fullname);
            } else {
                $markup .= $maker->fullname;
            }
        }

        return $markup;
    }

    /**
     * @param $number
     * @return string
     */
    public function moneyFormat($number)
    {
        return number_format($number, 2, ',', '') . '&euro;';
    }

    /**
     * @param string $hexColor
     * @return string
     */
    public function findColorInvert($hexColor)
    {
        if ($this->colorLuminance($hexColor) > 0.55) {
            return '#000';
        } else {
            return '#fff';
        }
    }

    /**
     * @param string $hexColor
     * @return float Return a decimal number between 0 and 1 where <= 0.5 is dark and > 0.5 is light.
     */
    public function colorLuminance($hexColor)
    {
        list($red, $green, $blue) = sscanf($hexColor, "#%02x%02x%02x");
        $rgbColor = compact('red', 'green', 'blue');

        foreach ($rgbColor as $name => $value) {
            $adjusted = 0;
            $value = $value / 255;

            if ($value < 0.03928) {
                $value = $value / 12.92;
            } else {
                $value = ($value + 0.055) / 1.055;
            }

            $rgbColor[$name] = $value;
        }

        return ($rgbColor['red'] * 0.2126) + ($rgbColor['green'] * 0.7152) + ($rgbColor['blue'] * 0.0722);
    }

    /**
     * @param string $hexColor
     * @param int $steps Steps should be between -255 and 255. Negative = darker, positive = lighter
     * @return string
     */
    public function adjustBrightness($hexColor, $steps)
    {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hexColor = str_replace('#', '', $hexColor);
        if (strlen($hexColor) == 3) {
            $hexColor = str_repeat(substr($hexColor,0,1), 2).str_repeat(substr($hexColor,1,1), 2).str_repeat(substr($hexColor,2,1), 2);
        }

        // Split into three parts: R, G and B
        $colorParts = str_split($hexColor, 2);
        $return = '#';

        foreach ($colorParts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }

    /**
     * @param string $input
     * @return string
     */
    public function cssMinify($input)
    {
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $input);
        $buffer = str_replace(': ', ':', $buffer);
        $output = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

        return $output;
    }
}

