<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class FilmCrudRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'title:es' => 'required',
            'title:gl' => 'required',
            'slug:es' => 'nullable|alpha_dash',
            'slug:gl' => 'nullable|alpha_dash',
            'makers[]' => '',
            'year' => 'required|numeric',
            'minutes' => 'required|numeric',
            'classification:es' => 'required',
            'classification:gl' => 'required',
            'production' => '',
            'cast' => '',
            'trailer:es' => '',
            'trailer:gl' => '',
            'header_image_file_path' => 'nullable|mimes:png,jpeg|dimensions:min_width=1384,min_height=800',
            'poster_file_path_es' => 'nullable|mimes:png,jpeg|dimensions:min_width=768,min_height=1099',
            'poster_file_path_gl' => 'nullable|mimes:png,jpeg|dimensions:min_width=768,min_height=1099',
            'release_date:es' => 'required',
            'release_date:gl' => 'required',
            'is_release_date_visible' => 'boolean',
            'original_language:es' => 'required',
            'original_language:gl' => 'required',
            'subtitles:es' => '',
            'subtitles:gl' => '',
            'origin_countries:es' => 'required',
            'origin_countries:gl' => 'required',
            'synopsis:es' => '',
            'synopsis:gl' => '',
            'is_homepage' => 'boolean',
            'homepage_sort' => 'required',
            'published' => 'boolean',
        ];
    }
}
