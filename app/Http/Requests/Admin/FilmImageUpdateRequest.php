<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class FilmImageUpdateRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'file_path' => 'nullable|mimes:png,jpeg|dimensions:min_width=768,min_height=432',
            'name:es' => '',
            'name:gl' => '',
        ];
    }
}
