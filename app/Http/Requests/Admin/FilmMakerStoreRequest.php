<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class FilmMakerStoreRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'slug' => 'nullable|alpha_dash',
            'photo_file_path' => 'required|mimes:png,jpeg|dimensions:min_width=768,min_height=1104',
            'biography:es' => '',
            'biography:gl' => '',
        ];
    }
}
