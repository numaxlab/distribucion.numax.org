<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BlogPostStoreRequest as StoreRequest;
use App\Http\Requests\Admin\BlogPostUpdateRequest as UpdateRequest;
use App\Models\BlogPost;
use App\Models\Film;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class BlogPostCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(BlogPost::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/blog-posts');
        $this->crud->setEntityNameStrings('post', 'posts');

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'title',
                'label' => 'Título'
            ],
            [
                'name' => 'published_at',
                'label' => 'Público desde'
            ],
            [
                'name' => 'is_homepage',
                'label' => 'En portada',
                'type' => 'boolean'
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Título ES',
                'name' => 'title:es'
            ],
            [
                'label' => 'Título GL',
                'name' => 'title:gl'
            ],
            [
                'label' => 'Slug ES',
                'name' => 'slug:es'
            ],
            [
                'label' => 'Slug GL',
                'name' => 'slug:gl'
            ],
            [
                'label' => 'Introdución ES',
                'name' => 'intro:es',
                'type' => 'wysiwyg'
            ],
            [
                'label' => 'Introdución GL',
                'name' => 'intro:gl',
                'type' => 'wysiwyg'
            ],
            [
                'label' => 'Corpo ES',
                'name' => 'body:es',
                'type' => 'wysiwyg'
            ],
            [
                'label' => 'Corpo GL',
                'name' => 'body:gl',
                'type' => 'wysiwyg'
            ],
            [
                'label' => 'Autoría',
                'name' => 'original_author'
            ],
            [
                'label' => 'Imaxe listado',
                'name' => 'image_file_path',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public',
                'hint' => 'Tamaño mínimo: 960x596',
            ],
            [
                'label' => 'Imaxe interior',
                'name' => 'inner_image_file_path',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public',
                'hint' => 'Tamaño mínimo: 960x596',
            ],
            [
                'label' => 'Ligazón da imaxe interior',
                'name' => 'inner_image_link',
                'type' => 'url'
            ],
            [
                'label' => 'Filmes relacionados',
                'name' => 'films',
                'type' => 'select2_multiple',
                'entity' => 'films',
                'attribute' => 'title',
                'model' => Film::class,
                'pivot' => true
            ],
            [
                'label' => 'Público desde',
                'name' => 'published_at',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm:ss',
                    'language' => 'es'
                ]
            ],
            [
                'label' => 'En portada',
                'name' => 'is_homepage',
                'type' => 'checkbox'
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
