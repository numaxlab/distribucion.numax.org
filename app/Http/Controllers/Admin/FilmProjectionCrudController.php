<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Http\Requests\Admin\FilmProjectionCrudRequest;
use App\Models\Cinema;
use App\Models\Film;
use App\Models\FilmProjection;

class FilmProjectionCrudController extends FilmNestedCrudController
{
    public function setup()
    {
        $filmId = $this->getFilmId();

        $this->crud->setModel(FilmProjection::class);
        $this->setRouteParameterName('projection');
        $this->setRouteSuffix('projections');
        $this->setEntityNameString('proxección', 'proxeccións');
        $this->addFilmClause();

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'label' => 'Filme',
                'type' => 'select',
                'name' => 'film_id',
                'entity' => 'film',
                'attribute' => 'title',
                'model' => Film::class
            ],
            [
                'label' => 'Sala',
                'type' => 'select',
                'name' => 'cinema_id',
                'entity' => 'cinema',
                'attribute' => 'name',
                'model' => Cinema::class
            ],
            [
                'label' => 'Data de inicio',
                'type' => 'datetime',
                'name' => 'starts_at',
            ],
            [
                'label' => 'Data de fin',
                'type' => 'datetime',
                'name' => 'ends_at',
            ],
            [
                'name' => 'published',
                'label' => 'Pública',
                'type' => 'boolean'
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Sala',
                'name' => 'cinema_id',
                'type' => 'select2',
                'entity' => 'cinema',
                'attribute' => 'name',
                'model' => Cinema::class
            ],
            [
                'label' => 'Data de inicio',
                'name' => 'starts_at',
                'type' => 'date_picker',
                'date_picker_options' => [
                    'format' => 'yyyy-mm-dd',
                    'language' => App::getLocale()
                ]
            ],
            [
                'label' => 'Data de fin',
                'name' => 'ends_at',
                'type' => 'date_picker',
                'date_picker_options' => [
                    'format' => 'yyyy-mm-dd',
                    'language' => App::getLocale()
                ]
            ],
            [
                'label' => 'Info ES',
                'name' => 'info:es'
            ],
            [
                'label' => 'Info GL',
                'name' => 'info:gl'
            ],
            [
                'label' => 'URL compra de entradas',
                'name' => 'ticket_purchase_url',
                'type' => 'url'
            ],
            [
                'label' => 'Pública',
                'name' => 'published',
                'type' => 'checkbox'
            ],
            [
                'name' => 'film_id',
                'type' => 'hidden',
                'value' => $filmId
            ]
        ]);

        $this->crud->enableReorder('summary', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmProjectionCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmProjectionCrudRequest $request)
    {
        return parent::updateCrud();
    }
}
