<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FilmMakerStoreRequest;
use App\Http\Requests\Admin\FilmMakerUpdateRequest;
use App\Models\FilmMaker;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class FilmMakerCrudController extends CrudController
{

    public function setup()
    {
        $this->crud->setModel(FilmMaker::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/film-makers');
        $this->crud->setEntityNameStrings('cineasta', 'cineastas');

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
            ],
            [
                'name' => 'surname',
                'label' => 'Apelidos',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Nome',
                'name' => 'name'
            ],
            [
                'label' => 'Apelidos',
                'name' => 'surname'
            ],
            [
                'label' => 'Slug',
                'name' => 'slug'
            ],
            [
                'label' => 'Foto',
                'name' => 'photo_file_path',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public',
                'hint' => 'Tamaño mínimo: 768x1104',
            ],
            [
                'label' => 'Pé de foto ES',
                'name' => 'photo_caption:es',
            ],
            [
                'label' => 'Pé de foto GL',
                'name' => 'photo_caption:gl',
            ],
            [
                'label' => 'Biografía ES',
                'name' => 'biography:es',
                'type' => 'wysiwyg'
            ],
            [
                'label' => 'Biografía GL',
                'name' => 'biography:gl',
                'type' => 'wysiwyg'
            ]
        ]);

        $this->crud->enableReorder('fullname', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmMakerStoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmMakerUpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
