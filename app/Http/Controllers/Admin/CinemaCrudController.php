<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CinemaCrudRequest as StoreRequest;
use App\Http\Requests\Admin\CinemaCrudRequest as UpdateRequest;
use App\Models\Cinema;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class CinemaCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Cinema::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/cinemas');
        $this->crud->setEntityNameStrings('sala', 'salas');

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
            ],
            [
                'name' => 'city',
                'label' => 'Cidade'
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Nome',
                'name' => 'name'
            ],
            [
                'label' => 'Cidade',
                'name' => 'city'
            ],
            [
                'label' => 'Web',
                'name' => 'website_url',
                'type' => 'url'
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
