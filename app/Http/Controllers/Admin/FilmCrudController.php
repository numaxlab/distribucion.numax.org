<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Http\Requests\Admin\FilmCrudRequest;
use App\Models\Film;
use App\Models\FilmMaker;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class FilmCrudController extends CrudController
{

    public function setup()
    {
        $this->crud->setModel(Film::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/films');
        $this->crud->setEntityNameStrings('filme', 'filmes');
        $this->crud->addButtonFromView('line', 'film_images', 'film_images', 'end');
        $this->crud->addButtonFromView('line', 'film_awards', 'film_awards', 'end');
        $this->crud->addButtonFromView('line', 'film_quotes', 'film_quotes', 'end');
        $this->crud->addButtonFromView('line', 'film_resources', 'film_resources', 'end');
        $this->crud->addButtonFromView('line', 'film_projections', 'film_projections', 'end');
        $this->crud->addButtonFromView('line', 'film_streaming_media', 'film_streaming_media', 'end');

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'title',
                'label' => 'Título',
            ],
            [
                'label' => 'Cineasta(s)',
                'type' => 'select_multiple',
                'name' => 'makers',
                'entity' => 'makers',
                'attribute' => 'fullname',
                'model' => FilmMaker::class,
            ],
            [
                'name' => 'published',
                'label' => 'Publicado',
                'type' => 'boolean'
            ],
            [
                'name' => 'vod_sort',
                'label' => 'Posición VoD (orde)',
                'type'=> 'number'
            ],
            [
                'name' => 'is_homepage',
                'label' => 'En portada',
                'type' => 'boolean'
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Título ES',
                'name' => 'title:es',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Título GL',
                'name' => 'title:gl',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Slug ES',
                'name' => 'slug:es',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Slug GL',
                'name' => 'slug:gl',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Cineasta(s)',
                'name' => 'makers',
                'type' => 'select2_multiple',
                'entity' => 'makers',
                'attribute' => 'fullname',
                'model' => FilmMaker::class,
                'pivot' => true,
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Ano',
                'name' => 'year',
                'type' => 'number',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Embed do trailer ES',
                'name' => 'trailer:es',
                'type' => 'textarea',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Embed do trailer GL',
                'name' => 'trailer:gl',
                'type' => 'textarea',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Cartel ES',
                'name' => 'poster_file_path_es',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public',
                'hint' => 'Tamaño mínimo: 768x1099',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Cartel GL',
                'name' => 'poster_file_path_gl',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public',
                'hint' => 'Tamaño mínimo: 768x1099',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Estrea ES',
                'name' => 'release_date:es',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Estrea GL',
                'name' => 'release_date:gl',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Mostrar estrea',
                'name' => 'is_release_date_visible',
                'type'=> 'checkbox',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Sinopse ES',
                'name' => 'synopsis:es',
                'type' => 'wysiwyg',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Sinopse GL',
                'name' => 'synopsis:gl',
                'type' => 'wysiwyg',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'En portada',
                'name' => 'is_homepage',
                'type'=> 'checkbox',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Orde en portada',
                'name' => 'homepage_sort',
                'type'=> 'number',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Publicado',
                'name' => 'published',
                'type'=> 'checkbox',
                'tab' => 'Datos do filme'
            ],
            [
                'label' => 'Duración',
                'name' => 'minutes',
                'type' => 'number',
                'suffix' => 'minutos',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Calificación ES',
                'name' => 'classification:es',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Calificación GL',
                'name' => 'classification:gl',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Idioma orixinal ES',
                'name' => 'original_language:es',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Idioma orixinal GL',
                'name' => 'original_language:gl',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Subtítulos ES',
                'name' => 'subtitles:es',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Subtítulos GL',
                'name' => 'subtitles:gl',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'País(es) de orixe ES',
                'name' => 'origin_countries:es',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'País(es) de orixe GL',
                'name' => 'origin_countries:gl',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Produción',
                'name' => 'production',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Elenco',
                'name' => 'cast',
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Formatos de exhibición',
                'name' => 'exhibition_formats',
                'type' => 'table',
                'entity_singular' => 'formato',
                'columns' => [
                    'name' => 'Nome'
                ],
                'min' => 1,
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Redes de difusión',
                'name' => 'broadcastingNetworks',
                'type' => 'select2_multiple',
                'entity' => 'broadcastingNetworks',
                'attribute' => 'name',
                'model' => App\Models\BroadcastingNetwork::class,
                'pivot' => true,
                'tab' => 'Info técnica'
            ],
            [
                'label' => 'Imaxe de cabeceira',
                'name' => 'header_image_file_path',
                'hint' => 'Tamaño mínimo: 1384x800',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public',
                'tab' => 'Estilos'
            ],
            [
                'label' => 'Cor de texto da cabeceira',
                'name' => 'header_text_color',
                'type' => 'color_picker',
                'tab' => 'Estilos'
            ],
            [
                'label' => 'Cor de fondo',
                'name' => 'background_color',
                'type' => 'color_picker',
                'tab' => 'Estilos'
            ],
            [
                'label' => 'Cor do texto',
                'name' => 'text_color',
                'type' => 'color_picker',
                'tab' => 'Estilos'
            ],
            [
                'label' => 'Cor das ligazóns e botóns',
                'name' => 'primary_action_background_color',
                'type' => 'color_picker',
                'tab' => 'Estilos'
            ],
            [
                'label' => 'Activar VoD',
                'name' => 'has_vod',
                'type'=> 'checkbox',
                'tab' => 'Ver na casa'
            ],
            [
                'label' => 'URL ficha filme',
                'name' => 'vod_film_link',
                'type' => 'url',
                'tab' => 'Ver na casa'
            ],
            [
                'label' => 'Embed VoD',
                'name' => 'vod_embed',
                'type' => 'textarea',
                'tab' => 'Ver na casa'
            ],
            [
                'label' => 'URL VoD',
                'name' => 'vod_link',
                'type' => 'url',
                'tab' => 'Ver na casa'
            ],
            [
                'label' => 'Prezo aluguer',
                'name' => 'vod_price',
                'type' => 'number',
                'attributes' => ['step' => 'any'],
                'suffix' => '€',
                'tab' => 'Ver na casa'
            ],
            [
                'label' => 'Prezo compra',
                'name' => 'vod_buy_price',
                'type' => 'number',
                'attributes' => ['step' => 'any'],
                'suffix' => '€',
                'tab' => 'Ver na casa'
            ],
            [
                'label' => 'Posición VoD (orde)',
                'name' => 'vod_sort',
                'type'=> 'number',
                'tab' => 'Ver na casa'
            ],
        ]);

        $this->crud->enableReorder('title', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmCrudRequest $request)
    {
        return parent::updateCrud();
    }
}
