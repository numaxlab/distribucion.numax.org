<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FilmResourceCrudRequest;
use App\Models\Film;
use App\Models\FilmResource;

class FilmResourceCrudController extends FilmNestedCrudController
{
    public function setup()
    {
        $filmId = $this->getFilmId();

        $this->crud->setModel(FilmResource::class);
        $this->setRouteParameterName('resource');
        $this->setRouteSuffix('resources');
        $this->setEntityNameString('material', 'materiais');
        $this->addFilmClause();

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'label' => 'Filme',
                'type' => 'select',
                'name' => 'film_id',
                'entity' => 'film',
                'attribute' => 'title',
                'model' => Film::class
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Nome ES',
                'name' => 'name:es'
            ],
            [
                'label' => 'Nome GL',
                'name' => 'name:gl'
            ],
            [
                'label' => 'Arquivo',
                'name' => 'file_path',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public'
            ],
            [
                'label' => 'Ligazón',
                'name' => 'url',
                'type' => 'url'
            ],
            [
                'name' => 'film_id',
                'type' => 'hidden',
                'value' => $filmId
            ]
        ]);

        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmResourceCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmResourceCrudRequest $request)
    {
        return parent::updateCrud();
    }
}
