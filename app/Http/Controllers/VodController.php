<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Repositories\PageMetaTagsBag;
use Illuminate\Http\Request;
use ModelManager;

class VodController extends Controller
{
    /**
     * @var \App\Repositories\FilmRepository $filmRepository
     */
    private $filmRepository;

    /**
     * VodController constructor.
     * @param PageMetaTagsBag $pageMetaTagsBag
     */
    public function __construct(PageMetaTagsBag $pageMetaTagsBag)
    {
        parent::__construct($pageMetaTagsBag);

        $this->filmRepository = ModelManager::getRepository(Film::class);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $films = $this->filmRepository->paginateVod();

        $this->pageMetaTagsBag
            ->setTitle(__('messages.secciones.titulo-vod'));

        return view('web.pages.vod.index', compact('films'));
    }
}
