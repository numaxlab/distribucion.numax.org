<?php

namespace App\Http\Controllers;

use App;
use App\Models\BlogPost;
use App\Models\Film;
use App\Models\FilmMaker;
use App\Models\Page;
use App\Repositories\Criteria\ByTranslatedSlug;
use App\Repositories\Criteria\IsPublishedNow;
use App\Repositories\Criteria\SortBy;
use App\Repositories\PageMetaTagsBag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelLocalization;
use ModelManager;
use ViewHelper;

class MainController extends Controller
{
    /**
     * @var \App\Repositories\FilmRepository
     */
    private $filmRepository;

    /**
     * @var \Ablunier\Laravel\Database\Contracts\Repository\Repository
     */
    private $filmMakerRepository;

    /**
     * @var \App\Repositories\BlogPostRepository
     */
    private $blogPostRepository;

    public function __construct(\App\Repositories\PageMetaTagsBag $pageMetaTagsBag) {
        parent::__construct($pageMetaTagsBag);

        $this->filmRepository = ModelManager::getRepository(Film::class);
        $this->filmMakerRepository = ModelManager::getRepository(FilmMaker::class);
        $this->blogPostRepository = ModelManager::getRepository(BlogPost::class);
    }

    public function homepage()
    {
        $films = $this->filmRepository->findAllInHomepage(['makers']);

        $blogPosts = $this->blogPostRepository->findAllInHomepage();

        return view('web.pages.homepage', compact('films', 'blogPosts'));
    }

    public function page(Request $request)
    {
        $pageSlug = $request->segment(2);

        $pageRepository = ModelManager::getRepository(Page::class);

        /** @var Page $page */
        $page = $pageRepository->pushCriteria(new ByTranslatedSlug($pageSlug))
            ->firstOrFail();

        $this->pageMetaTagsBag
            ->setTitle($page->name);

        return view('web.pages.page.single', compact('page'));
    }

    public function sitemap()
    {
        /** @var \Roumen\Sitemap\Sitemap $sitemap */
        $sitemap = App::make("sitemap");

        foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties) {
            App::setLocale($localeCode);

            $sitemap->add(
                LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'homepage')),
                Carbon::now()->toAtomString(),
                '0.8',
                'weekly'
            );

            $films = $this->filmRepository->findAllPublished();

            if ($films->isNotEmpty()) {
                $sitemap->add(
                    LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'film.index')),
                    $films->first()->updated_at->toAtomString(),
                    '0.9',
                    'weekly'
                );

                foreach ($films as $film) {
                    $videos = [];
                    if (! empty($film->trailer)) {
                        $videos = [
                            [
                                'title' => __('messages.info-trailer', ['film' => $film->title]),
                                'description' => str_limit($film->synopsis, 2048),
                            ]
                        ];
                    }

                    $sitemap->add(
                        LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'film.show', ['slug' => $film->slug])),
                        $film->updated_at->toAtomString(),
                        '1.0',
                        'weekly',
                        [],
                        null,
                        [],
                        $videos
                    );
                }
            }

            $filmMakers = $this->filmMakerRepository
                ->pushCriteria(new SortBy('lft'))
                ->all();

            if ($filmMakers->isNotEmpty()) {
                $sitemap->add(
                    LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'film-maker.index')),
                    $filmMakers->first()->updated_at->toAtomString(),
                    '0.9',
                    'monthly'
                );

                foreach ($filmMakers as $filmMaker) {
                    $sitemap->add(
                        LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'film-maker.show', ['slug' => $filmMaker->slug])),
                        $filmMaker->updated_at->toAtomString(),
                        '1.0',
                        'monthly'
                    );
                }
            }

            $blogPosts = $this->blogPostRepository
                ->pushCriteria(new IsPublishedNow())
                ->pushCriteria(new SortBy('published_at', 'DESC'))
                ->all();

            if ($blogPosts->isNotEmpty()) {
                $sitemap->add(
                    LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'blog-post.index')),
                    $blogPosts->first()->updated_at->toAtomString(),
                    '0.8',
                    'weekly'
                );

                foreach ($blogPosts as $blogPost) {
                    $sitemap->add(
                        LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'blog-post.show', ['slug' => $blogPost->slug])),
                        $blogPost->updated_at->toAtomString(),
                        '0.9',
                        'weekly'
                    );
                }
            }

            $sitemap->add(
                LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'film.submit')),
                Carbon::now()->toAtomString(),
                '0.7',
                'never'
            );

            $sitemap->add(
                LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode, 'about')),
                Carbon::now()->toAtomString(),
                '0.7',
                'yearly'
            );
        }

        return $sitemap->render('xml');
    }
}
