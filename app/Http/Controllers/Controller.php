<?php

namespace App\Http\Controllers;

use App\Repositories\PageMetaTagsBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var \App\Repositories\PageMetaTagsBag
     */
    protected $pageMetaTagsBag;

    public function __construct(PageMetaTagsBag $pageMetaTagsBag)
    {
        $this->pageMetaTagsBag = $pageMetaTagsBag;

        $this->pageMetaTagsBag
            ->setText(__('messages.secciones.descripcion-general'))
            ->setImage(asset('assets/web/img/og-image.jpg'))
            ->setTwitterUser('@numaxfilms');
    }
}
