<?php

namespace App\Http\Controllers;

use App\Http\Requests\HostFilmRequest;
use App\Http\Requests\SubmitFilmRequest;
use App\Models\Film;
use App\Models\User;
use App\Notifications\FilmHostRequested;
use App\Notifications\FilmSubmitted;
use App\Repositories\Criteria\ByTranslatedSlug;
use App\Repositories\Criteria\IsPublished;
use App\Repositories\PageMetaTagsBag;
use Carbon\Carbon;
use Dominiquevienne\Honeypot\Form;
use App\Services\Honeypot;
use ModelManager;
use Notification;
use Illuminate\Http\Request;
use ViewHelper;

class FilmController extends Controller
{
    /**
     * @var \App\Repositories\FilmRepository $filmRepository
     */
    private $filmRepository;

    public function __construct(PageMetaTagsBag $pageMetaTagsBag) {
        parent::__construct($pageMetaTagsBag);

        $this->filmRepository = ModelManager::getRepository(Film::class);
    }

    public function index(Request $request)
    {
        $films = $this->filmRepository->paginateAllPublished(['makers']);

        if ($request->ajax()) {
            $content = view('web.pages.film.list-ajax', compact('films'))
                ->render();

            return [
                'has_more' => $films->hasMorePages(),
                'next' => $films->nextPageUrl(),
                'content' => $content
            ];
        }

        $this->pageMetaTagsBag
            ->setTitle(__('messages.secciones.titulo-peliculas'));

        return view('web.pages.film.list', compact('films'));
    }

    public function show($slug)
    {
        /** @var Film $film */
        $film = $this->filmRepository->pushCriteria(new ByTranslatedSlug($slug))
            ->pushCriteria(new IsPublished())
            ->firstOrFail([
                'projections' => function ($query) {
                    $query->where('published', true)
                        ->whereDate('starts_at', '<=', Carbon::now()->toDateString())
                        ->whereDate('ends_at', '>=', Carbon::now()->toDateString())
                        ->orderBy('lft')
                        ->with(['cinema']);
                },
                'makers', 'quotes', 'images', 'resources',
                'streamingMedia', 'awards', 'broadcastingNetworks', 'blogPosts'
            ]);

        $film->broadcastingNetworks = $film->broadcastingNetworks->sortBy('name');

        $description = $film->title.', '.lcfirst(__('messages.autor-pelicula', ['author' => ViewHelper::showFilmFilmMakers($film->makers)]));

        $this->pageMetaTagsBag
            ->setTitle($film->title)
            ->setText($description)
            ->setImage((string) ViewHelper::imgSrc($film, 'header_image_file_path', 'resizeCrop,637,333'));

        $honeypot = new Form([
            'formMethod' => 'POST'
        ]);

        $honeypot->timeCheck();

        return view('web.pages.film.single', compact('film', 'honeypot'));
    }

    public function permalink($slug)
    {
        $film = $this->filmRepository->pushCriteria(new ByTranslatedSlug($slug))
            ->pushCriteria(new IsPublished())
            ->firstOrFail();

        return redirect()
            ->route('film.show', $film->slug);
    }

    public function styles($slug)
    {
        /** @var Film $film */
        $film = $this->filmRepository->pushCriteria(new ByTranslatedSlug($slug))
            ->pushCriteria(new IsPublished())
            ->firstOrFail();

        $css = view('web.pages.film.styles', compact('film'))
            ->render();

        if (! config('app.debug')) {
            $css = ViewHelper::cssMinify($css);
        }

        return response($css)
            ->header('Content-Type', 'text/css');
    }

    public function postHostRequest(HostFilmRequest $request, $slug)
    {
        $honeypot = new Honeypot();

        if ($honeypot->checks() !== true) {
            return redirect()
                ->route('homepage');
        }

        /** @var Film $film */
        $film = $this->filmRepository->pushCriteria(new ByTranslatedSlug($slug))
            ->pushCriteria(new IsPublished())
            ->firstOrFail();

        $users = User::all();

        Notification::send($users, new FilmHostRequested($film, $request));

        return redirect()
            ->route('film.show', $film->slug)
            ->with('flash-message', __('messages.formularios.peticion-proyeccion-recibida'));
    }

    public function submit()
    {
        $this->pageMetaTagsBag
            ->setTitle(__('messages.secciones.titulo-envia-tu-pelicula'))
            ->setText(__('messages.envia-pelicula-info'));

        $honeypot = new Form([
            'formMethod' => 'POST'
        ]);

        $honeypot->timeCheck();

        return view('web.pages.film.submit', compact('honeypot'));
    }

    public function postSubmit(SubmitFilmRequest $request)
    {
        $honeypot = new Honeypot();

        if ($honeypot->checks() !== true) {
            return redirect()
                ->route('homepage');
        }

        $users = User::all();

        Notification::send($users, new FilmSubmitted($request));

        return redirect()
            ->route('film.submit')
            ->with('flash-message', __('messages.formularios.envia-pelicula-recibida'));
    }
}
