<?php


namespace App\Repositories\Criteria;


use Ablunier\Laravel\Database\Contracts\Repository\Criteria;
use Ablunier\Laravel\Database\Contracts\Repository\Repository;
use Carbon\Carbon;

class IsPublishedNow implements Criteria
{

    public function apply($model, Repository $repository)
    {
        return $model->where('published_at', '<=', Carbon::now()->format('Y-m-d H:i:s'));
    }
}
