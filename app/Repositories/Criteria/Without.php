<?php


namespace App\Repositories\Criteria;


use Ablunier\Laravel\Database\Contracts\Repository\Criteria;
use Ablunier\Laravel\Database\Contracts\Repository\Repository;

class Without implements Criteria
{
    /**
     * @var array
     */
    private $ids;

    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    public function apply($model, Repository $repository)
    {
        return $model->whereNotIn('id', $this->ids);
    }
}
