<?php


namespace App\Repositories\Criteria;


use Ablunier\Laravel\Database\Contracts\Repository\Criteria;
use Ablunier\Laravel\Database\Contracts\Repository\Repository;

class SortBy implements Criteria
{
    /**
     * @var string
     */
    private $field;
    /**
     * @var string
     */
    private $direction;

    public function __construct($field, $direction = 'ASC')
    {
        $this->field = $field;
        $this->direction = $direction;
    }

    public function apply($model, Repository $repository)
    {
        return $model->orderBy($this->field, $this->direction);
    }
}
