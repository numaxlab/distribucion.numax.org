<?php

namespace App\Services;

class Honeypot extends \Dominiquevienne\Honeypot\Honeypot
{

    /**
     * Honeypot constructor.
     */
    public function __construct()
    {
        $config = [
            'minFormCompletionTime' => 10
        ];

        $this->setLogPath(storage_path('logs/honeypot.log'));

        parent::__construct($config);
    }
}
