<?php

namespace App\Providers;

use App\Repositories\PageMetaTagsBag;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale($this->app->getLocale());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\ViewHelper', function ($app) {
            return new \App\Helpers\ViewHelper();
        });

        $this->app->singleton('App\Repositories\PageMetaTagsBag', function ($app) {
            return new PageMetaTagsBag();
        });

        if ($this->app->environment() === 'local') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
    }
}
