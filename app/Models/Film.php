<?php

namespace App\Models;

use Ablunier\Laravel\Database\Contracts\Repository\HasCustomRepository;
use App\Models\Traits\UploadFilesTrait;
use App\Repositories\FilmRepository;
use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Film
 *
 * @property int $id
 * @property int $year
 * @property int $minutes
 * @property array $exhibition_formats
 * @property string $production
 * @property string $cast
 * @property array $trailer
 * @property string $header_image_file_path
 * @property string $poster_file_path
 * @property bool $published
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $film_maker_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmAward[] $awards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BlogPost[] $blogPosts
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\BroadcastingNetwork[] $broadcastingNetworks
 * @property-read mixed $exhibition_formats_collection
 * @property-read mixed $trailer_poster
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmImage[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmProjection[] $projections
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmQuote[] $quotes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmResource[] $resources
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmStreamingMedia[] $streamingMedia
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereCast($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereClassification($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereExhibitionFormats($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereHeaderImageFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereMinutes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film wherePosterFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereProduction($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereReleaseDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereTrailer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film withTranslation()
 * @mixin \Eloquent
 * @property string $header_text_color
 * @property string $background_color
 * @property string $text_color
 * @property string $primary_action_background_color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmMaker[] $makers
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereBackgroundColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereHeaderTextColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film wherePrimaryActionBackgroundColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film wherePrimaryActionTextColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereTextColor($value)
 * @property bool $is_homepage
 * @property bool $is_release_date_visible
 * @property-read mixed $trailer_video_url
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereIsHomepage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereIsReleaseDateVisible($value)
 * @property string $primary_action_text_color
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereRgt($value)
 */
class Film extends Model implements HasCustomRepository
{
    use Translatable, CrudTrait, UploadFilesTrait;

    const STORAGE_DISK = 'public';
    const UPLOADS_DESTINATION_PATH = 'filmes';

    public $translatedAttributes = [
        'title',
        'slug',
        'original_language',
        'subtitles',
        'origin_countries',
        'synopsis',
        'classification',
        'release_date',
        'trailer',
        'poster_file_path',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'film_maker_id',
        'year',
        'is_release_date_visible',
        'published',
        'is_homepage',
        'homepage_sort',
        'minutes',
        'production',
        'cast',
        'exhibition_formats',
        'poster_file_path_es',
        'poster_file_path_gl',
        'header_image_file_path',
        'header_text_color',
        'background_color',
        'text_color',
        'primary_action_background_color',
        'has_vod',
        'vod_film_link',
        'vod_embed',
        'vod_link',
        'vod_price',
        'vod_buy_price',
        'vod_sort',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'exhibition_formats' => 'array',
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    public function makers()
    {
        return $this->belongsToMany(FilmMaker::class, 'film_film_maker', 'film_id', 'film_maker_id')
            ->orderBy('lft');
    }

    public function projections()
    {
        return $this->hasMany(FilmProjection::class);
    }

    public function quotes()
    {
        return $this->hasMany(FilmQuote::class)
            ->orderBy('lft');
    }

    public function images()
    {
        return $this->hasMany(FilmImage::class)
            ->orderBy('lft');
    }

    public function resources()
    {
        return $this->hasMany(FilmResource::class)
            ->orderBy('lft');
    }

    public function streamingMedia()
    {
        return $this->hasMany(FilmStreamingMedia::class)
            ->orderBy('lft');
    }

    public function awards()
    {
        return $this->hasMany(FilmAward::class)
            ->orderBy('lft');
    }

    public function broadcastingNetworks()
    {
        return $this->belongsToMany(BroadcastingNetwork::class);
    }

    public function blogPosts()
    {
        return $this->belongsToMany(BlogPost::class)
            ->orderBy('published_at');
    }

    public function setPosterFilePathEsAttribute($value)
    {
        $this->storeFileToDisk($value, 'poster_file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH, 'es');
    }

    public function setPosterFilePathGlAttribute($value)
    {
        $this->storeFileToDisk($value, 'poster_file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH, 'gl');
    }

    public function getPosterFilePathEsAttribute()
    {
        $translation = $this->getTranslation('es');

        if (empty($translation)) {
            return null;
        }

        return $translation->getAttribute('poster_file_path');
    }

    public function getPosterFilePathGlAttribute()
    {
        $translation = $this->getTranslation('gl');

        if (empty($translation)) {
            return null;
        }

        return $translation->getAttribute('poster_file_path');
    }

    public function setHeaderImageFilePathAttribute($value)
    {
        $this->storeFileToDisk($value, 'header_image_file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH);
    }

    public function getExhibitionFormatsCollectionAttribute()
    {
        if (empty($this->exhibition_formats) || empty($this->exhibition_formats[0])) {
            return collect();
        }

        return collect($this->exhibition_formats);
    }

    public function repository()
    {
        return FilmRepository::class;
    }
}
