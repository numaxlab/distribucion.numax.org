<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmProjection
 *
 * @property int $id
 * @property \Carbon\Carbon $starts_at
 * @property \Carbon\Carbon $ends_at
 * @property string $ticket_purchase_url
 * @property bool $published
 * @property int $film_id
 * @property int $cinema_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Cinema $cinema
 * @property-read \App\Models\Film $film
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmProjectionTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereCinemaId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereEndsAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereFilmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereStartsAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereTicketPurchaseUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection withTranslation()
 * @mixin \Eloquent
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @property-read mixed $summary
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjection whereRgt($value)
 */
class FilmProjection extends Model
{
    use Translatable, CrudTrait;

    public $translatedAttributes = ['info'];

    protected $fillable = [
        'starts_at',
        'ends_at',
        'ticket_purchase_url',
        'published',
        'film_id',
        'cinema_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['starts_at', 'ends_at'];

    public function film()
    {
        return $this->belongsTo(Film::class);
    }

    public function cinema()
    {
        return $this->belongsTo(Cinema::class);
    }

    public function getSummaryAttribute()
    {
        return $this->cinema->name.': '.$this->starts_at->format('d/m/Y').' - '.$this->ends_at->format('d/m/Y');
    }
}
