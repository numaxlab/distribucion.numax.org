<?php

namespace App\Models;

use App\Models\Traits\UploadFilesTrait;
use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmResource
 *
 * @property int $id
 * @property string $file_path
 * @property string $url
 * @property int $film_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Film $film
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmResourceTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereFilmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource withTranslation()
 * @mixin \Eloquent
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResource whereRgt($value)
 */
class FilmResource extends Model
{
    use Translatable, CrudTrait, UploadFilesTrait;

    const STORAGE_DISK = 'public';
    const UPLOADS_DESTINATION_PATH = 'filmes/materiais';

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'file_path',
        'url',
        'film_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    public function film()
    {
        return $this->belongsTo(Film::class);
    }

    public function setFilePathAttribute($value)
    {
        $this->storeFileToDisk($value, 'file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH);
    }
}
