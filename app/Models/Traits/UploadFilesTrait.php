<?php

namespace App\Models\Traits;

use Request;
use Storage;

trait UploadFilesTrait
{
    protected $storageDisk = 'public';

    /**
     * Handle file upload and DB storage for a file:
     * - on CREATE
     *     - stores the file at the destination path
     *     - generates a name
     *     - stores the full path in the DB;
     * - on UPDATE
     *     - if the value is null, deletes the file and sets null in the DB
     *     - if the value is different, stores the different file and updates DB value.
     *
     * @param $value
     * @param $attributeName
     * @param $disk
     * @param $destinationPath
     * @param null $translationLang
     * @internal param $ [type] $value            Value for that column sent from the input.
     * @internal param $ [type] $attributeName    Model attribute name (and column in the db).
     * @internal param $ [type] $disk             Filesystem disk used to store files.
     * @internal param $ [type] $destinationPath  Path in disk where to store the files.
     */
    public function storeFileToDisk($value, $attributeName, $disk, $destinationPath, $translationLang = null)
    {
        $request = Request::instance();

        if (isset($translationLang)) {
            $uploadedFileName = $attributeName.'_'.$translationLang;
            $model = $this->getTranslation($translationLang);
        } else {
            $uploadedFileName = $attributeName;
            $model = $this;
        }

        // if a new file is uploaded, delete the file from the disk
        if ($request->hasFile($uploadedFileName) &&
            $model->{$attributeName} &&
            $model->{$attributeName} != null
        ) {
            Storage::disk($disk)->delete($model->{$attributeName});
            $model->attributes[$attributeName] = null;
        }

        // if the file input is empty, delete the file from the disk
        if (is_null($value) && $model->{$attributeName} != null) {
            Storage::disk($disk)->delete($model->{$attributeName});
            $model->attributes[$attributeName] = null;
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($uploadedFileName) && $request->file($uploadedFileName)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($uploadedFileName);
            $newFileName = str_replace($file->getClientOriginalExtension(), '', str_slug($file->getClientOriginalName())).'-'.uniqid().'.'.$file->getClientOriginalExtension();

            // 2. Move the new file to the correct path
            $filePath = $file->storeAs($destinationPath, $newFileName, $disk);

            // 3. Save the complete path to the database
            $model->attributes[$attributeName] = $filePath;
        }
    }

    /**
     * Handle multiple file upload and DB storage:
     * - if files are sent
     *     - stores the files at the destination path
     *     - generates random names
     *     - stores the full path in the DB, as JSON array;
     * - if a hidden input is sent to clear one or more files
     *     - deletes the file
     *     - removes that file from the DB.
     *
     * @param  [type] $value            Value for that column sent from the input.
     * @param  [type] $attribute_name   Model attribute name (and column in the db).
     * @param  [type] $disk             Filesystem disk used to store files.
     * @param  [type] $destination_path Path in disk where to store the files.
     */
    public function storeMultipleFilesToDisk($value, $attributeName, $disk, $destinationPath)
    {
        $request = Request::instance();
        $attributeValue = (array) $this->{$attributeName};
        $filesToClear = $request->get('clear_'.$attributeName);

        // if a file has been marked for removal,
        // delete it from the disk and from the db
        if ($filesToClear) {
            $attributeValue = (array) $this->{$attributeName};
            foreach ($filesToClear as $key => $filename) {
                Storage::disk($disk)->delete($filename);
                $attributeValue = array_where($attributeValue, function ($value, $key) use ($filename) {
                    return $value != $filename;
                });
            }
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attributeName)) {
            foreach ($request->file($attributeName) as $file) {
                if ($file->isValid()) {
                    // 1. Generate a new file name
                    $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

                    // 2. Move the new file to the correct path
                    $file_path = $file->storeAs($destinationPath, $new_file_name, $disk);

                    // 3. Add the public path to the database
                    $attributeValue[] = $file_path;
                }
            }
        }

        $this->attributes[$attributeName] = json_encode($attributeValue);
    }
}
