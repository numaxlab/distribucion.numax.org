<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmQuote
 *
 * @property int $id
 * @property string $media
 * @property string $url
 * @property int $film_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Film $film
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmQuoteTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereFilmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereMedia($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote withTranslation()
 * @mixin \Eloquent
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuote whereRgt($value)
 */
class FilmQuote extends Model
{
    use Translatable, CrudTrait;

    public $translatedAttributes = ['quote'];

    protected $fillable = [
        'media',
        'url',
        'film_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    public function film()
    {
        return $this->belongsTo(Film::class);
    }
}
