<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmStreamingMedia
 *
 * @property int $id
 * @property string $url
 * @property string $embed
 * @property bool $starred
 * @property int $film_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Film $film
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmStreamingMediaTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereEmbed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereFilmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereStarred($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia withTranslation()
 * @mixin \Eloquent
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMedia whereRgt($value)
 */
class FilmStreamingMedia extends Model
{
    use Translatable, CrudTrait;

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'url',
        'embed',
        'starred',
        'film_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    public function film()
    {
        return $this->belongsTo(Film::class);
    }
}
