<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmMakerTranslation
 *
 * @property int $id
 * @property int $film_maker_id
 * @property string $locale
 * @property string $biography
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMakerTranslation whereBiography($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMakerTranslation whereFilmMakerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMakerTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMakerTranslation whereLocale($value)
 * @mixin \Eloquent
 * @property string $photo_caption
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMakerTranslation whereImageCaption($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMakerTranslation wherePhotoCaption($value)
 */
class FilmMakerTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'biography',
        'photo_caption'
    ];
}
