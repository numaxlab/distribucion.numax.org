<?php

namespace App\Models;

use App\Models\Traits\UploadFilesTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * App\Models\FilmMaker
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $slug
 * @property string $photo_file_path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @property-read mixed $fullname
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmMakerTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker wherePhotoFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereSurname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker withTranslation()
 * @mixin \Eloquent
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmMaker whereRgt($value)
 */
class FilmMaker extends Model
{
    use Translatable, Sluggable, CrudTrait, UploadFilesTrait;

    const STORAGE_DISK = 'public';
    const UPLOADS_DESTINATION_PATH = 'cineastas';

    public $translatedAttributes = ['biography', 'photo_caption'];

    protected $fillable = [
        'name',
        'surname',
        'photo_file_path'
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    public static function boot() {
        parent::boot();

        static::deleting(function ($model) {
            Storage::disk(self::STORAGE_DISK)->delete($model->photo_file_path);
        });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'fullname'
            ]
        ];
    }

    public function films()
    {
        return $this->belongsToMany(Film::class, 'film_film_maker', 'film_maker_id', 'film_id')
            ->orderBy('lft');
    }

    public function setPhotoFilePathAttribute($value)
    {
        $this->storeFileToDisk($value, 'photo_file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH);
    }

    public function getFullnameAttribute()
    {
        return $this->name.' '.$this->surname;
    }
}
