<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PageTranslation
 *
 * @property int $id
 * @property int $page_id
 * @property string $locale
 * @property string $name
 * @property string $slug
 * @property string $content
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PageTranslation findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PageTranslation whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PageTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PageTranslation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PageTranslation wherePageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PageTranslation whereSlug($value)
 * @mixin \Eloquent
 * @property-read mixed $slug_source
 */
class PageTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'content'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_source',
                'unique' => false
            ]
        ];
    }

    public function getSlugSourceAttribute()
    {
        if (! empty(trim($this->slug))) {
            return $this->slug;
        }

        return $this->name;
    }
}
