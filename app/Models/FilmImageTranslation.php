<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmImageTranslation
 *
 * @property int $id
 * @property int $film_image_id
 * @property string $locale
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImageTranslation whereFilmImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImageTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImageTranslation whereName($value)
 * @mixin \Eloquent
 */
class FilmImageTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
