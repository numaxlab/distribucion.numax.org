<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmProjectionTranslation
 *
 * @property int $id
 * @property int $film_projection_id
 * @property string $locale
 * @property string $info
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjectionTranslation whereFilmProjectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjectionTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjectionTranslation whereInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmProjectionTranslation whereLocale($value)
 * @mixin \Eloquent
 */
class FilmProjectionTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['info'];
}
