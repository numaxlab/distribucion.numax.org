<?php

namespace App\Models;

use Ablunier\Laravel\Database\Contracts\Repository\HasCustomRepository;
use App\Models\Traits\UploadFilesTrait;
use App\Repositories\BlogPostRepository;
use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BlogPost
 *
 * @property int $id
 * @property string $image_file_path
 * @property \Carbon\Carbon $published_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BlogPostTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereImageFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost wherePublishedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost withTranslation()
 * @mixin \Eloquent
 * @property string $original_author
 * @property bool $is_homepage
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereIsHomepage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPost whereOriginalAuthor($value)
 */
class BlogPost extends Model implements HasCustomRepository
{
    use Translatable, CrudTrait, UploadFilesTrait;

    const STORAGE_DISK = 'public';
    const UPLOADS_DESTINATION_PATH = 'blog';

    public $translatedAttributes = ['title', 'slug', 'intro', 'body'];

    protected $fillable = [
        'image_file_path',
        'inner_image_file_path',
        'inner_image_link',
        'published_at',
        'original_author',
        'is_homepage',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    public function films()
    {
        return $this->belongsToMany(Film::class)
            ->orderBy('lft');
    }

    public function setImageFilePathAttribute($value)
    {
        $this->storeFileToDisk($value, 'image_file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH);
    }

    public function setInnerImageFilePathAttribute($value)
    {
        $this->storeFileToDisk($value, 'inner_image_file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH);
    }

    /**
     * Get Eloquent Model custom repository.
     *
     * @return string
     */
    public function repository()
    {
        return BlogPostRepository::class;
    }
}
