<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'registration_closed'  => 'El registro de usuarios está cerrado.',
    'first_page_you_see'   => 'Bienvenido al panel de administración',
    'login_status'         => 'Estado de la conexión',
    'logged_in'            => '¡Has iniciado sesión!',
    'toggle_navigation'    => 'Mostrar/ocultar menú de navegación',
    'administration'       => 'ADMINISTRACIÓN',
    'user'                 => 'USUARIO',
    'logout'               => 'Cerrar sesión',
    'login'                => 'Iniciar sesión',
    'register'             => 'Crear usuario',
    'name'                 => 'Nombre',
    'email_address'        => 'Correo electrónico',
    'password'             => 'Contraseña',
    'confirm_password'     => 'Confirmar contraseña',
    'remember_me'          => 'Recordar contraseña',
    'forgot_your_password' => '¿Olvidaste tu contraseña?',
    'reset_password'       => 'Restaurar contraseña',
    'send_reset_link'      => 'Enviar enlace para restaurar la contraseña',
    'click_here_to_reset'  => 'Haz clic aquí para restaurar la contraseña',
    'unauthorized'         => 'No autorizado.',
    'dashboard'            => 'Panel',
    'handcrafted_by'       => 'Desarrollado por',
    'powered_by'           => 'Creado con',
    'homepage'             => 'Ir a la web'
];
