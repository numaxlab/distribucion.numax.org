<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'registration_closed'  => 'O rexistro de usuarios non está permitido.',
    'first_page_you_see'   => 'Benvido ao panel de administración',
    'login_status'         => 'Estado da conexión',
    'logged_in'            => 'Iniciaches sesión!',
    'toggle_navigation'    => 'Mostrar/ocultar o menú de navegación',
    'administration'       => 'ADMINISTRACIÓN',
    'user'                 => 'USUARIO',
    'logout'               => 'Pechar sesión',
    'login'                => 'Iniciar sesión',
    'register'             => 'Crear usuario',
    'name'                 => 'Nome',
    'email_address'        => 'Correo electrónico',
    'password'             => 'Contrasinal',
    'confirm_password'     => 'Confirmar contrasinal',
    'remember_me'          => 'Recordar contrasinal',
    'forgot_your_password' => 'Esqueciches o teu contrasinal?',
    'reset_password'       => 'Restaurar contrasinal',
    'send_reset_link'      => 'Enviar ligazón para restaurar o contrasinal',
    'click_here_to_reset'  => 'Fai clic aquí para restaurar o contrasinal',
    'unauthorized'         => 'Non autorizado.',
    'dashboard'            => 'Panel',
    'handcrafted_by'       => 'Feito por',
    'powered_by'           => 'Creado con',
    'homepage'             => 'Ir á web'
];
