<?php

return [
    'homepage' => '/',
    'film.index' => '/peliculas',
    'film.show' => '/peliculas/{slug}',
    'film.styles' => '/peliculas/{slug}/styles.css',
    'film.host-request.post' => '/peliculas/{slug}/reservar-proyeccion',
    'film-maker.index' => '/cineastas',
    'film-maker.show' => '/cineastas/{slug}',
    'blog-post.index' => '/cuaderno',
    'blog-post.show' => '/cuaderno/{slug}',
    'film.submit' => 'envia-tu-pelicula',
    'film.submit.post' => 'envia-tu-pelicula/enviar',
    'about' => 'quienes-somos',
    'privacy-policy' => 'politica-de-privacidad',
    'cookie-policy' => 'politica-de-cookies',
    'vod.index' => 'en-casa',
];
