<?php

return [
    'homepage' => '/',
    'film.index' => '/filmes',
    'film.show' => '/filmes/{slug}',
    'film.styles' => '/filmes/{slug}/styles.css',
    'film.host-request.post' => '/filmes/{slug}/reservar-proxeccion',
    'film-maker.index' => '/cineastas',
    'film-maker.show' => '/cineastas/{slug}',
    'blog-post.index' => '/caderno',
    'blog-post.show' => '/caderno/{slug}',
    'film.submit' => 'envia-o-teu-filme',
    'film.submit.post' => 'envia-o-teu-filme/enviar',
    'about' => 'quen-somos',
    'privacy-policy' => 'politica-de-privacidade',
    'cookie-policy' => 'politica-de-cookies',
    'vod.index' => 'na-casa',
];
