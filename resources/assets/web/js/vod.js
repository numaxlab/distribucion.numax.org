$('.vod-modal-opener').on('click', function (event) {
  event.preventDefault();

  let target = $(this).data('target');
  $(target).addClass('is-active');
});

$('.modal-close').on('click', function (event) {
  $($(this).data('target')).removeClass('is-active');
});
