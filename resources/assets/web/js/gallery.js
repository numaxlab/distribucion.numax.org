class Gallery {
    constructor(items, index) {
        this._items = items;
        this._index = index;
    }

    current() {
        return this._items[this._index];
    }

    next() {
        this._index++;

        if (this._index >= this._items.length) {
            this._index = 0;
        }
    }

    prev() {
        this._index--;

        if (this._index < 0) {
            this._index = this._items.length - 1;
        }
    }
}

module.exports = Gallery;
