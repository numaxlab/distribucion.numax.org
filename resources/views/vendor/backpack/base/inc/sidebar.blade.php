@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> {{ trans('backpack::base.logout') }}</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix').'/films') }}"><i class="fa fa-film"></i> <span>Filmes</span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix').'/film-makers') }}"><i class="fa fa-id-card-o"></i> <span>Cineastas</span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix').'/cinemas') }}"><i class="fa fa-television"></i> <span>Salas</span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix').'/broadcasting-networks') }}"><i class="fa fa-sitemap"></i> <span>Redes de difusión</span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix').'/blog-posts') }}"><i class="fa fa-newspaper-o"></i> <span>Blog</span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix').'/pages') }}"><i class="fa fa-file-text-o"></i> <span>Páxinas</span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix').'/elfinder') }}"><i class="fa fa-files-o"></i> <span>Xestor de arquivos</span></a></li>

          <li class="treeview">
            <a href="#"><i class="fa fa-globe"></i> <span>Traduccións</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/language') }}"><i class="fa fa-flag-checkered"></i> Idiomas</a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/language/texts') }}"><i class="fa fa-language"></i> Textos da web</a></li>
            </ul>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
