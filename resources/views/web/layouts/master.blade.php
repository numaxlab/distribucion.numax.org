<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $page->getTitle() }}</title>

        @foreach ($page->html() as $meta)
            {!! $meta !!}
        @endforeach

        @foreach ($page->openGraph() as $meta)
            {!! $meta !!}
        @endforeach

        @foreach ($page->twitterCard() as $meta)
            {!! $meta !!}
        @endforeach

        @section('css')
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
            <link href="{{ mix('/assets/web/css/app.css') }}" rel="stylesheet">
        @show

        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('manifest.json') }}">
        <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="#000000">
        <meta name="theme-color" content="#ffffff">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "WebSite",
              "name": "NUMAX Distribucion",
              "url": "{{ Request::url() }}"
            }
        </script>

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

        @yield('html-head')
    </head>
    <body class="@yield('body-classes')">
        {!! file_get_contents(resource_path('assets/web/img/logos-sprite.svg')) !!}

        @section('header')
            @include('web.partials.nav.default')
        @show

        @section('main-container')
        <div class="container main-container">
            @include('web.partials.validation-errors')
            @include('web.partials.flash-message')

            @yield('content')
        </div>
        @show

        @section('footer')
            @include('web.partials.footer.default')
        @show

        @include('web.partials.cookie-message')

        @section('js')
        <script src="{{ mix('/assets/web/js/manifest.js') }}"></script>
        <script src="{{ mix('/assets/web/js/vendor.js') }}"></script>
        <script src="{{ mix('/assets/web/js/app.js') }}"></script>
        @show

        @include('web.partials.google-analytics')
    </body>
</html>