@extends('web.layouts.master')

@section('main-container')
    @yield('content')
@endsection
