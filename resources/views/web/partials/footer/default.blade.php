<footer class="footer">
    <div class="container">
        <div class="columns">
            <div class="column is-half columns is-desktop">
                <div class="column columns is-mobile is-gapless">
                    <div class="column is-narrow">
                        <svg class="logo-n">
                            <use xlink:href="#logo-n-white"></use>
                        </svg>
                    </div>

                    <div class="column">
                        <address class="address">
                            <span class="name">NOCTURAMA, S.L.</span><br />
                            Domingo García-Sabell, 3-4B<br />
                            15705 Santiago de Compostela<br />
                            A Coruña
                        </address>

                        <a href="https://www.facebook.com/numaxdistribucion" target="_blank" class="icon icon-facebook-square">
                            <span class="is-hidden">Facebook</span>
                        </a>
                        <a href="https://twitter.com/numaxfilms" target="_blank" class="icon icon-twitter-square">
                            <span class="is-hidden">Twitter</span>
                        </a>
                        <a href="https://www.instagram.com/numaxdistribucion" target="_blank" class="icon icon-instagram">
                            <span class="is-hidden">Instagram</span>
                        </a>
                        <a href="mailto:distribucion@numax.org" class="icon icon-envelope-o">
                            <span class="is-hidden">Email</span>
                        </a>
                    </div>
                </div>

                <ul class="column site-map">
                    <li>
                        <a href="{{ route('film.index') }}">
                            {{ __('messages.secciones.titulo-peliculas') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('film-maker.index') }}">
                            {{ __('messages.secciones.titulo-cineastas') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('vod.index') }}">
                            {{ __('messages.secciones.titulo-vod') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('blog-post.index') }}">
                            {{ __('messages.secciones.titulo-cuaderno') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('film.submit') }}">
                            {{ __('messages.secciones.titulo-envia-tu-pelicula') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('about') }}">
                            {{ __('messages.secciones.titulo-quien-somos') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('privacy-policy') }}">
                            {{ __('messages.secciones.titulo-politica-privacidad') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cookie-policy') }}">
                            {{ __('messages.secciones.titulo-politica-cookies') }}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="column is-half"></div>
        </div>
    </div>
</footer>