<div class="cookie-message">
    <p>{!! __('messages.cookies', ['url' => route('cookie-policy')]) !!}</p>
    <a href="#" class="cookie-message-close button">{{ __('messages.cookies-aceptar') }}</a>
</div>