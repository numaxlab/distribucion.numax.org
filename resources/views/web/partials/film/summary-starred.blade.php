<article class="film-summary-starred" style="background-image: url('{{ ViewHelper::imgSrc($film, 'header_image_file_path', 'resizeCrop,1344,612') }}');" itemscope itemtype="http://schema.org/Movie">
    <a href="{{ route('film.show', $film->slug) }}" class="film-link" itemprop="url">
        <div>
            <h2 class="title is-2"@if (! empty($film->header_text_color)) style="color: {{ $film->header_text_color }};"@endif>
                {{ $film->title }}
            </h2>
            <p class="subtitle is-4"@if (! empty($film->header_text_color)) style="color: {{ $film->header_text_color }};"@endif>
                {{ __('messages.autor-pelicula', ['author' => ViewHelper::showFilmFilmMakers($film->makers)]) }}
            </p>
        </div>
        @if ($film->is_release_date_visible)
            <div class="film-release-date">
                <span class="text">
                    {{ $film->release_date }}
                </span>
            </div>
        @endif
    </a>
</article>