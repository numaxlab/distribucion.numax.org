@if (count($errors) > 0)
    <div class="notification is-danger">
        <button class="delete"></button>
        <p class="description">{{ __('messages.formularios.titulo-errores') }}:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
