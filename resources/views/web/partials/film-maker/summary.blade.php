<article class="film-maker-summary" itemscope itemtype="http://schema.org/Person">
    <a href="{{ route('film-maker.show', $filmMaker->slug) }}" class="film-maker-link" itemprop="url">
        <div class="image is-1by1">
            <img src="{{ ViewHelper::imgSrc($filmMaker, 'photo_file_path', 'resizeCrop,768,768') }}" alt="" />
        </div>
        <h3 class="title is-4">{{ $filmMaker->fullname }}</h3>
    </a>
</article>