<nav class="nav">
    <div class="container">
        <div class="nav-left">
            <a href="{{ route('homepage') }}" class="nav-item nav-logo">
                @section('header-logo')
                    <svg class="logo-dist">
                        <use xlink:href="#logo-dist"></use>
                    </svg>
                @show
            </a>
        </div>

        <div class="nav-right">
            <div class="nav-lang-toggle">
                <div class="nav-item">
                    <span></span>
                    <span>{{ LaravelLocalization::getCurrentLocale() }}</span>
                    <span></span>
                </div>
            </div>

            <ul class="nav-lang">
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <li class="nav-item {{ active_class((LaravelLocalization::getCurrentLocale() == $localeCode), 'is-active') }}">
                    <a hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, ViewHelper::getLocalizedUrl($localeCode)) }}" rel="alternate" title="{{ $properties['native'] }}">
                        {{ $localeCode }}
                    </a>
                </li>
                @endforeach
            </ul>

            <span class="nav-toggle" data-target=".nav-menu">
                <span></span>
                <span></span>
                <span></span>
            </span>

            <ul class="nav-menu">
                <li class="nav-item {{ active_class(if_route(['film.index', 'film.show']), 'is-active') }}">
                    <a href="{{ route('film.index') }}">
                        {{ __('messages.secciones.titulo-peliculas') }}
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route(['film-maker.index', 'film-maker.show']), 'is-active') }}">
                    <a href="{{ route('film-maker.index') }}">
                        {{ __('messages.secciones.titulo-cineastas') }}
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route(['vod.index']), 'is-active') }}">
                    <a href="{{ route('vod.index') }}">
                        {{ __('messages.secciones.titulo-vod') }}
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route(['blog-post.index', 'blog-post.show']), 'is-active') }}">
                    <a href="{{ route('blog-post.index') }}">
                        {{ __('messages.secciones.titulo-cuaderno') }}
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route(['film.submit']), 'is-active') }}">
                    <a href="{{ route('film.submit') }}">
                        {{ __('messages.secciones.titulo-envia-tu-pelicula') }}
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route(['about']), 'is-active') }}">
                    <a href="{{ route('about') }}">
                        {{ __('messages.secciones.titulo-quien-somos') }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>