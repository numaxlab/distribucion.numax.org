@extends("web.layouts.master")

@section('body-classes', 'pg-vod-index')

@section('header-logo')
    <svg class="logo-dist">
        <use xlink:href="#logo-dist-white"></use>
    </svg>
@endsection

@section("content")
    <section class="films">
        <h1 class="is-hidden">{{ __('messages.secciones.titulo-vod') }}</h1>

        @if (! $films->isEmpty())
            <ul class="films-list columns is-mobile is-multiline">
                @foreach ($films as $film)
                    <li class="column is-half-mobile is-one-quarter-tablet">
                        @include("web.partials.film.summary-vod", ['film' => $film])
                    </li>
                @endforeach
            </ul>

            @if ($films->hasMorePages())
                <div class="pagination-more columns">
                    <div class="column is-6 is-offset-3">
                        <a href="" class="button button-more" data-target=".films-list" data-url="{{ $films->nextPageUrl() }}">{{ __('messages.acciones.ver-mas') }}</a>
                    </div>
                </div>
            @endif
        @endif
    </section>
@endsection

@section('js')
    @parent
    <script src="{{ mix('/assets/web/js/vod.js') }}"></script>
@endsection
