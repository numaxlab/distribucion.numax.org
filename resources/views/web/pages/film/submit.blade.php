@extends('web.layouts.master')

@section('content')
    <section class="submit-film">
        <h1 class="is-hidden">{{ __('messages.secciones.titulo-envia-tu-pelicula') }}</h1>

        <div class="columns">
            <div class="column form-aside-info">
                <p>{!! __('messages.envia-pelicula-info') !!}</p>

                <p>* {{ __('messages.formularios.campo-obligatorio') }}</p>
            </div>
            <form action="{{ route('film.submit.post') }}" method="post" class="column is-two-thirds">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                {!! $honeypot->inputs() !!}
                <div class="field">
                    <label class="label" for="name-input">{{ ucfirst(__('validation.attributes.name')) }}</label>
                    <div class="control">
                        <input type="text" name="name" class="input" id="name-input" required="required" placeholder="{{ ucfirst(__('validation.attributes.name')) }} *" />
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="surname-input">{{ ucfirst(__('validation.attributes.surname')) }}</label>
                    <div class="control">
                        <input type="text" name="surname" class="input" id="surname-input" required="required" placeholder="{{ ucfirst(__('validation.attributes.surname')) }} *" />
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="email-input">{{ ucfirst(__('validation.attributes.email')) }}</label>
                    <div class="control">
                        <input type="email" name="email" class="input" id="email-input" required="required" placeholder="{{ ucfirst(__('validation.attributes.email')) }} *" />
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="institution-input">{{ ucfirst(__('validation.attributes.institution')) }}</label>
                    <div class="control">
                        <input type="text" name="institution" class="input" id="institution-input" placeholder="{{ ucfirst(__('validation.attributes.institution')) }}" />
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="film-title-input">{{ ucfirst(__('validation.attributes.film_title')) }}</label>
                    <div class="control">
                        <input type="text" name="film_title" class="input" id="film-title-input" placeholder="{{ ucfirst(__('validation.attributes.film_title')) }} *" required="required" />
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="film-link-input">{{ ucfirst(__('validation.attributes.film_link')) }}</label>
                    <div class="control">
                        <input type="url" name="film_link" class="input" id="film-link-input" placeholder="{{ ucfirst(__('validation.attributes.film_link')) }} *" required="required" />
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="film-info-textarea">{{ ucfirst(__('validation.attributes.film_info')) }}</label>
                    <div class="control">
                        <textarea name="film_info" id="film-info-textarea" placeholder="{{ ucfirst(__('validation.attributes.film_info')) }}" class="textarea"></textarea>
                    </div>
                </div>
                <div class="field">
                    <label class="checkbox">
                        <input type="checkbox" name="privacy_policy" required>
                        {!! __('messages.formularios.privacy-policy', ['href' => route('privacy-policy')]) !!}
                    </label>
                </div>
                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-primary">{{ __('messages.acciones.enviar-formulario') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
