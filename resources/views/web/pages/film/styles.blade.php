@if (! empty($film->background_color))
    .page-film {
        background-color: {{ $film->background_color }};
    }

    .film .film-release-date {
        background-color: {{ $film->background_color }};
        color: {{ ViewHelper::findColorInvert($film->background_color) }};
    }

    .film .film-projections .table {
        background-color: {{ ViewHelper::adjustBrightness($film->background_color, -10) }};
    }

    .film .button {
        background-color: {{ $film->background_color }};
    }
@endif

@if (! empty($film->text_color))
    .film {
        color: {{ $film->text_color }};
    }

    .film .film-release-date {
        color: {{ $film->text_color }};
    }

    .film .subtitle {
        color: {{ $film->text_color }};
    }

    .film strong {
        color: {{ $film->text_color }};
    }

    .film .content {
        color: {{ $film->text_color }};
    }

    .film .table {
        color: {{ $film->text_color }};
    }

    .film .film-block .film-block-header .title {
        color: {{ $film->text_color }};
    }

    .film .blog-post-summary .intro {
        color: {{ $film->text_color }};
    }
@endif

@if (! empty($film->header_image_file_path))
    .film .film-header {
        background-image: url("{{ ViewHelper::imgSrc($film, 'header_image_file_path') }}");
    }
@endif

@if (! empty($film->header_text_color))
    .film .film-header .title {
        color: {{ $film->header_text_color }};
    }
@endif

@if (! empty($film->primary_action_background_color))
    <?php $hoverColor = ViewHelper::adjustBrightness($film->primary_action_background_color, -20); ?>

    .film a {
        color: {{ $film->primary_action_background_color }};
    }

    .film a:hover,
    .film a:active,
    .film a:focus {
        color: {{ $hoverColor }};
    }

    .film .button {
        border-color: {{ $film->primary_action_background_color }};
    }

    .film .button:hover,
    .film .button:active,
    .film .button:focus {
        border-color: {{ $hoverColor }};
    }

    .film .button.is-primary {
        background-color: {{ $film->primary_action_background_color }};
        color: {{ ViewHelper::findColorInvert($film->primary_action_background_color) }};
    }

    .film .button.is-primary:hover,
    .film .button.is-primary:active,
    .film .button.is-primary:focus {
        background-color: {{ $hoverColor }};
        color: {{ ViewHelper::findColorInvert($hoverColor) }};
    }

    .film .owl-carousel a {
        color: {{ $film->primary_action_background_color }};
    }

    .film .owl-carousel a:hover,
    .film .owl-carousel a:focus,
    .film .owl-carousel a:active {
        color: {{ $hoverColor }};
    }

    .film .owl-carousel .owl-nav [class*="owl-"] {
        color: {{ $film->primary_action_background_color }};
        border-color: {{ $film->primary_action_background_color }};
    }

    .film .owl-carousel .owl-nav [class*="owl-"]:hover,
    .film .owl-carousel .owl-nav [class*="owl-"]:active,
    .film .owl-carousel .owl-nav [class*="owl-"]:focus {
        color: {{ $hoverColor }};
        border-color: {{ $hoverColor }};
    }

    .film .film-block.is-collapsible .film-block-header .title,
    .film .film-block.is-collapsible .film-block-header a {
        color: {{ $film->primary_action_background_color }};
    }

    .film .film-block.is-collapsible .film-block-header:hover .title,
    .film .film-block.is-collapsible .film-block-header:hover a {
        color: {{ $hoverColor }};
    }

    .film .blog-post-summary .title {
        color: {{ $film->primary_action_background_color }};
    }

    .film .blog-post-summary .blog-post-link:hover .title,
    .film .blog-post-summary .blog-post-link:active .title,
    .film .blog-post-summary .blog-post-link:focus .title {
        color: {{ $hoverColor }};
    }
@endif
