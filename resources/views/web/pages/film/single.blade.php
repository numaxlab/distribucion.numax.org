@extends('web.layouts.film')

@section('css')
    @parent

    <link href="{{ route('film.styles', $film->slug) }}" rel="stylesheet">
@endsection

@section('body-classes', 'page-film')

@section('header')
    @parent

    <div class="nav-opener">
        <svg class="logo-n">
            <use xlink:href="#logo-n"></use>
        </svg>
    </div>
@endsection

@section('content')
    <article class="film" itemscope itemtype="http://schema.org/Movie">
        <header class="film-header">
            <div class="container">
                <h1 class="title is-1" itemprop="name">{{ $film->title }}</h1>
                @if ($film->is_release_date_visible)
                <div class="film-release-date">
                    <span class="text">
                        {{ $film->release_date }}
                    </span>
                </div>
                @endif
            </div>
        </header>

        <div class="container">
            <div class="film-info columns">
                <div class="column">
                    <h2 class="subtitle is-3" itemprop="director" itemscope itemtype="http://schema.org/Person">{{ __('messages.de') }} {!! ViewHelper::showFilmFilmMakers($film->makers, true) !!}</h2>

                    <p class="film-origin-countries" itemprop="countryOfOrigin">{{ $film->origin_countries }}</p>

                    <p class="film-year-duration">{{ $film->year }}, <span itemprop="duration">{{ $film->minutes }} {{ __('messages.minutos') }}</span>, {{ $film->classification }}</p>

                    @if (! empty($film->synopsis))
                    <div class="film-synopsis content" itemprop="description">
                        {!! $film->synopsis !!}
                        <?php /*<a href="#" class="more">{{ __('messages.leer-mas') }}</a>
                        <a href="#" class="less">{{ __('messages.cerrar') }}</a>*/ ?>
                    </div>
                    @endif
                </div>
                <div class="column">
                    <div class="columns">
                        <div class="column">
                            @if ($film->has_vod)
                                @include("web.partials.film.summary-vod", ['hideTitle' => true])
                            @else
                                <img src="{{ ViewHelper::imgSrc($film, 'poster_file_path', 'resizeCrop,768,1099') }}" alt="{{ __('messages.info-cartel', ['film' => $film->title]) }}" class="film-poster" itemprop="thumbnailUrl" />
                            @endif
                        </div>
                        <div class="column film-tech-info">
                            <ul>
                                @if (! empty($film->production))
                                <li>{{ __('messages.produccion') }}: <strong itemprop="productionCompany">{{ $film->production }}</strong></li>
                                @endif
                                @if (! empty($film->cast))
                                <li>{{ __('messages.elenco') }}: <strong>{{ $film->cast }}</strong></li>
                                @endif
                                <li>{{ __('messages.idioma-original') }}: <strong>{{ $film->original_language }}</strong></li>
                                @if (! empty($film->subtitles))
                                <li>{{ __('messages.subtitulos') }}: <strong>{{ $film->subtitles }}</strong></li>
                                @endif
                            </ul>

                            @if (! $film->broadcastingNetworks->isEmpty())
                            <div class="film-broadcasting-networks">
                                <p>{{ __('messages.redes-difusion-disponibles') }}:</p>
                                <ul>
                                    @foreach ($film->broadcastingNetworks as $broadcastingNetwork)
                                    <li>
                                        @if ($broadcastingNetwork->url)
                                        <a href="{{ $broadcastingNetwork->url }}" class="button is-primary" target="_blank">
                                            {{ $broadcastingNetwork->name }}
                                        </a>
                                        @else
                                        <span class="button is-primary">
                                            {{ $broadcastingNetwork->name }}
                                        </span>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            @if (! $film->awards->isEmpty())
            <div class="film-block film-awards">
                <div class="film-block-content">
                    <ul>
                        @foreach ($film->awards as $award)
                            <li class="image" itemprop="award"><img src="{{ ViewHelper::imgSrc($award, 'logo_file_path') }}" alt="{{ $award->name }}" class="film-award-img" /></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif

            @if (! $film->projections->isEmpty())
                <div class="film-block is-collapsible film-projections">
                    <div class="film-block-header">
                        <h3 class="title is-4">
                            {{ __('messages.salas-pelicula') }}
                            <span class="icon icon-angle-down"></span>
                        </h3>
                    </div>
                    <div class="film-block-content is-hidden">
                        <table class="table">
                            <thead class="is-hidden">
                            <tr>
                                <td>{{ __('messages.sala-ciudad') }}</td>
                                <td>{{ __('messages.sala-nombre') }}</td>
                                <td>{{ __('messages.sala-info') }}</td>
                                <td>{{ __('messages.sala-enlace-compra') }}</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($film->projections as $projection)
                                <tr>
                                    <td>{{ $projection->cinema->city }}</td>
                                    <td>
                                        @if (! empty($projection->cinema->website_url))
                                            <a href="{{ $projection->cinema->website_url }}" target="_blank">{{ $projection->cinema->name }}</a>
                                        @else
                                            {{ $projection->cinema->name }}
                                        @endif
                                    </td>
                                    <td>{{ $projection->info }}</td>
                                    <td>
                                        @if (! empty($projection->ticket_purchase_url))
                                            <a href="{{ $projection->ticket_purchase_url }}" target="_blank">{{ __('messages.sala-enlace-compra') }}</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif

            @if (! $film->streamingMedia->isEmpty())
                <div class="film-block is-collapsible film-streaming-media">
                    <div class="film-block-header">
                        <h3 class="title is-4">
                            {{ __('messages.en-casa-pelicula') }}
                            <span class="icon icon-angle-down"></span>
                        </h3>
                    </div>
                    <div class="film-block-content is-hidden">
                        <ul class="columns is-multiline">
                            @foreach ($film->streamingMedia as $streamingMedia)
                                <li class="column is-one-third" itemprop="potentialAction" itemscope itemtype="http://schema.org/WatchAction">
                                    <a
                                            @if (! empty($streamingMedia->url))
                                            href="{{ $streamingMedia->url }}" target="_blank" class="button is-primary"
                                            @else
                                            href="#" class="button is-primary streaming-media-modal-opener" data-target="#streaming-media-modal{{ $streamingMedia->id }}"
                                            @endif itemprop="target">
                                <span class="icon">
                                    <i class="icon-play-circle-o"></i>
                                </span>
                                        <span>{{ $streamingMedia->name }}</span>
                                    </a>
                                    <div class="modal" id="streaming-media-modal{{ $streamingMedia->id }}">
                                        <div class="modal-background"></div>
                                        <div class="modal-content">
                                            <div class="video-container">
                                                {!! $streamingMedia->embed !!}
                                            </div>
                                        </div>
                                        <button class="modal-close" data-target="#streaming-media-modal{{ $streamingMedia->id }}"></button>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <div class="film-block is-collapsible film-host-request">
                <div class="film-block-header">
                    <h3 class="title is-4">
                        <a name="film-host-request">
                            {{ __('messages.reserva-proyeccion-pelicula') }}
                            <span class="icon icon-angle-down"></span>
                        </a>
                    </h3>
                </div>
                <div class="film-block-content{{ (count($errors) === 0 && empty(session('flash-message'))) ? ' is-hidden' : '' }}">
                    @include('web.partials.validation-errors')
                    @include('web.partials.flash-message')

                    <div class="columns">
                        <form action="{{ route('film.host-request.post', $film->slug) }}" method="post" class="column is-two-thirds">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            {!! $honeypot->inputs() !!}
                            <div class="field">
                                <div class="control">
                                    <input type="text" name="contact_person" placeholder="{{ ucfirst(__('validation.attributes.contact_person')) }} *" class="input" />
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" name="cinema" placeholder="{{ ucfirst(__('validation.attributes.cinema')) }} *" class="input" />
                                </div>
                            </div>
                            @if ($film->exhibition_formats_collection->isNotEmpty())
                                <div class="field">
                                    <div class="control">
                                    <span class="select">
                                        <select name="format" class="select">
                                            <option value="">{{ ucfirst(__('validation.attributes.format')) }}</option>
                                            @foreach ($film->exhibition_formats as $format)
                                                <option value="{{ $format['name'] }}">{{ $format['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </span>
                                    </div>
                                </div>
                            @endif
                            <div class="field">
                                <div class="control">
                                    <input type="tel" name="phone_number" placeholder="{{ ucfirst(__('validation.attributes.phone_number')) }}" class="input" />
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="email" name="email" placeholder="{{ ucfirst(__('validation.attributes.email')) }} *" class="input" />
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <textarea name="comments" placeholder="{{ ucfirst(__('validation.attributes.comments')) }}" class="textarea"></textarea>
                                </div>
                            </div>
                            <div class="field">
                                <label class="checkbox">
                                    <input type="checkbox" name="privacy_policy" required>
                                    {!! __('messages.formularios.privacy-policy', ['href' => route('privacy-policy')]) !!}
                                </label>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <button type="submit" class="button is-primary">{{ __('messages.acciones.enviar-formulario') }}</button>
                                </div>
                            </div>
                        </form>
                        <div class="column form-aside-info">
                            <p>{{ __('messages.calificacion-pelicula') }}: <strong>{{ $film->classification }}</strong></p>
                            @if ($film->exhibition_formats_collection->isNotEmpty())
                                <p>{{ __('messages.formatos-pelicula') }}: <strong>{{ implode(', ', $film->exhibition_formats_collection->pluck('name')->toArray()) }}</strong></p>
                            @endif
                            <p>{!! __('messages.info-reserva-proyeccion') !!}</p>
                            <p>* {{ __('messages.formularios.campo-obligatorio') }}</p>
                        </div>
                    </div>
                </div>
            </div>

            @if (! $film->quotes->isEmpty() || ! empty($film->trailer))
            <div class="film-block">
                <div class="film-block-content columns">
                    @if (! $film->quotes->isEmpty())
                    <div class="column is-one-third">
                        <ul class="owl-carousel owl-theme">
                        @foreach ($film->quotes as $quote)
                            <li>
                                <aside>
                                    <blockquote cite="{{ $quote->url }}">
                                        <a href="{{ $quote->url }}" target="_blank" class="title is-4">{{ $quote->quote }}</a>
                                    </blockquote>
                                    <footer>({{ $quote->media }})</footer>
                                </aside>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (! empty($film->trailer))
                    <div class="column">
                        <div class="video-container" itemprop="trailer">
                            {!! $film->trailer !!}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            @endif

            @if (! $film->images->isEmpty())
            <div class="film-block">
                <div class="film-block-content">
                    <ul class="columns is-multiline images-gallery">
                        @foreach ($film->images as $image)
                        <li class="column is-one-third">
                            <a href="{{ ViewHelper::imgSrc($image, 'file_path') }}" class="image is-16by9 image-modal-opener">
                                <img src="{{ ViewHelper::imgSrc($image, 'file_path', 'resizeCrop,768,432') }}" alt="{{ $image->name }}" />
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="modal is-gallery" id="image-modal">
                        <div class="modal-background"></div>
                        <div class="modal-content">
                            <span class="gallery-prev button"></span>
                            <span class="image">
                                <img src="" alt="" />
                            </span>
                            <span class="gallery-next button"></span>
                        </div>
                        <button class="modal-close" data-target="#image-modal"></button>
                    </div>
                </div>
            </div>
            @endif

            @if (! $film->blogPosts->isEmpty())
            <div class="film-block film-related-posts">
                <div class="film-block-header">
                    <h3 class="title is-4">{{ __('messages.relacionados-cuaderno-pelicula') }}</h3>
                </div>
                <section class="film-block-content">
                    <ul class="columns is-multiline">
                        @foreach ($film->blogPosts as $blogPost)
                        <li class="column is-one-third">
                            @include('web.partials.blog-post.summary', ['blogPost' => $blogPost])
                        </li>
                        @endforeach
                    </ul>
                </section>
            </div>
            @endif

            @if (! $film->resources->isEmpty())
                <div class="film-block film-resources">
                    <div class="film-block-header">
                        <h3 class="title is-4">{{ __('messages.descargas-pelicula') }}</h3>
                    </div>
                    <div class="film-block-content">
                        <ul class="columns is-multiline">
                            @foreach ($film->resources as $resource)
                                <li class="column is-half">
                                    <a class="button"
                                        @if (! empty($resource->url))
                                            href="{{ $resource->url }}" target="_blank"
                                        @else
                                            href="{{ ViewHelper::fileHref($resource->file_path) }}" target="_blank"
                                        @endif>
                                        <span class="icon">
                                            <i class="icon-arrow-circle-o-down"></i>
                                        </span>
                                        <span>{{ $resource->name }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </article>
@endsection

@section('js')
    @parent
    <script src="{{ mix('/assets/web/js/film.js') }}"></script>
@endsection
