@extends('web.layouts.master')

@section('content')
    <section class="starred-films">
        <h1 class="is-hidden">{{ __('messages.secciones.titulo-peliculas') }}</h1>


        @if ($films->isNotEmpty())
            <ul class="owl-carousel owl-theme">
            @foreach ($films as $film)
                <li>
                    @include('web.partials.film.summary-starred', ['film' => $film])
                </li>
            @endforeach
            </ul>
        @endif

        <div class="columns more-films">
            <div class="column is-6 is-offset-3">
                <a href="{{ route('film.index') }}" class="button">{{ __('messages.acciones.catalogo-peliculas') }}</a>
            </div>
        </div>
    </section>

    <section class="starred-blog-posts">
        <h1 class="title is-4">{{ __('messages.portada-titulo-cuaderno') }}</h1>
        <div class="columns is-gapless">
            @foreach ($blogPosts as $blogPost)
                <div class="column is-3">
                    @include('web.partials.blog-post.summary', ['blogPost' => $blogPost])
                </div>
                <div class="column is-1"></div>
            @endforeach
        </div>
    </section>
@endsection

@section('js')
    @parent
    <script src="{{ mix('/assets/web/js/homepage.js') }}"></script>
@endsection