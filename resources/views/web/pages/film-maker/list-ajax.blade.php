@foreach ($filmMakers as $filmMaker)
    <li class="column is-one-third">
        @include("web.partials.film-maker.summary", ['blogPost' => $filmMaker])
    </li>
@endforeach