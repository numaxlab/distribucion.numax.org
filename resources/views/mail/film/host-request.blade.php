@component('mail::message')
# {{ __('messages.reserva-proyeccion-titulo-notificacion', ['film_title' => $film->title]) }}

    @foreach($formData as $key => $value)
        - {{ $key }}: {{ empty($value) ? '--' : $value }}
    @endforeach
@endcomponent
