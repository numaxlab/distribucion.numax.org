<?php
namespace Deployer;

require 'recipe/laravel.php';

// Configuration

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('timezone', 'Europe/Madrid');

set('repository', 'git@gitlab.com:numaxlab/distribucion.numax.org.git');

//add('shared_files', []);
add('shared_dirs', [
    'public/storage'
]);

set('writable_mode', 'chmod'); // chmod, chown, chgrp or acl.
set('writable_chmod_mode', '0777');
set('writable_chmod_recursive', false);

// Servers
serverList('servers.yml');

// Tasks
desc('Upload compiled assets');
task('deploy:upload-assets', function () {
    $release = get('release_path');

    $assets = [
        'public/assets',
        'public/packages',
        'public/vendor',
        'public/mix-manifest.json'
    ];

    foreach ($assets as $path) {
        upload($path, $release.'/'.$path);
    }
});

before('deploy:vendors', 'deploy:upload-assets');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
