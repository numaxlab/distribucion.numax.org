<?php

Route::get(__('routes.homepage'), [
    'as' => 'homepage',
    'uses' => 'MainController@homepage'
]);

Route::get(__('routes.film.index'), [
    'as' => 'film.index',
    'uses' => 'FilmController@index'
]);

Route::get(__('routes.film.show'), [
    'as' => 'film.show',
    //'middleware' => 'secure.content:vimeo_player',
    'uses' => 'FilmController@show'
]);

Route::post(__('routes.film.host-request.post'), [
    'as' => 'film.host-request.post',
    'uses' => 'FilmController@postHostRequest'
]);

Route::get(__('routes.film.styles'), [
    'as' => 'film.styles',
    'uses' => 'FilmController@styles'
]);

Route::get(__('routes.film-maker.index'), [
    'as' => 'film-maker.index',
    'uses' => 'FilmMakerController@index'
]);

Route::get(__('routes.film-maker.show'), [
    'as' => 'film-maker.show',
    'uses' => 'FilmMakerController@show'
]);

Route::get(__('routes.vod.index'), [
    'as' => 'vod.index',
    'uses' => 'VodController@index'
]);

Route::get(__('routes.blog-post.index'), [
    'as' => 'blog-post.index',
    'uses' => 'BlogPostController@index'
]);

Route::get(__('routes.blog-post.show'), [
    'as' => 'blog-post.show',
    'uses' => 'BlogPostController@show'
]);

Route::get(__('routes.film.submit'), [
    'as' => 'film.submit',
    'uses' => 'FilmController@submit'
]);

Route::post(__('routes.film.submit.post'), [
    'as' => 'film.submit.post',
    'uses' => 'FilmController@postSubmit'
]);

Route::get(__('routes.about'), [
    'as' => 'about',
    'uses' => 'MainController@page'
]);

Route::get(__('routes.privacy-policy'), [
    'as' => 'privacy-policy',
    'uses' => 'MainController@page'
]);

Route::get(__('routes.cookie-policy'), [
    'as' => 'cookie-policy',
    'uses' => 'MainController@page'
]);

Route::get('{slug}', [
    'as' => 'film.show-permalink',
    'uses' => 'FilmController@permalink'
]);
