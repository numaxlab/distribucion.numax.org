const { mix } = require('laravel-mix');

mix.js('resources/assets/web/js/app.js', 'public/assets/web/js')
    .js('resources/assets/web/js/film.js', 'public/assets/web/js')
    .js('resources/assets/web/js/homepage.js', 'public/assets/web/js')
    .js('resources/assets/web/js/vod.js', 'public/assets/web/js')
    .version()
    .extract(['jquery', 'lodash']);

mix.sass('resources/assets/web/sass/app.scss', 'public/assets/web/css')
    .version();

mix.copyDirectory('resources/assets/web/img', 'public/assets/web/img')
    .copyDirectory('resources/assets/web/fonts/icomoon/fonts', 'public/assets/web/fonts')
    .copy('resources/assets/admin/ckeditor/config.js', 'public/vendor/backpack/ckeditor/config.js');

mix.browserSync(process.env.MIX_DOMAIN);
